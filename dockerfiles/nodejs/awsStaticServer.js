const path = require('path')
const util = require('util')

const express = require('express')
const static = require('ecstatic')
const serverStatic = require('serve-static')

const Minio = require('minio')
const S3FS = require('s3fs')
const uuid = require('uuid').v4
const fs = require('fs-extra')

const s3Options = {
  endpoint: process.env.MINIO_ENDPOINT || 'http://0.0.0.0:9001/foobar',
  accessKeyId: 'admin',
  secretAccessKey: 'password',
  s3BucketEndpoint: true,
  signatureVersion: 'v4',
  bucketName: process.env.AWS_S3_BUCKET || 'foobar'
}
const s3fs = new S3FS(s3Options.bucketName, s3Options)
const minioClient = new Minio.Client({
  endPoint: process.env.MINIO_HOST || '0.0.0.0',
  port: +(process.env.MINIO_PORT || 9001),
  secure: false,
  accessKey: s3Options.accessKeyId,
  secretKey: s3Options.secretAccessKey
})

minioClient.makeBucket = util.promisify(minioClient.makeBucket)
minioClient._bucketExists = minioClient.bucketExists
minioClient.bucketExists = (bucketName) => {
  return new Promise((resolve, reject) => {
    return minioClient._bucketExists(bucketName, (err) => {
      if (err && err.code === 'NoSuchBucket') {
        return resolve(false)
      } else if (err) {
        return reject(err)
      }
      return resolve(true)
    })
  })
}

const STATIC_FOLDER = `${__dirname}/.tmp/public`

async function copyDirFromAws(awsPath, destPath) {
  const files = await s3fs.readdir(awsPath)

  for (let file of files) {
    file = path.normalize(`${awsPath}/${file}`)
    const stat = await s3fs.stat(file)
    if (stat.isFile()) {
      const { Body } = await s3fs.readFile(file)
      fs.writeFileSync(path.normalize(`${destPath}/${file}`), Body)
    }
    if (stat.isDirectory()) {
      const dir = path.normalize(`${destPath}/${file}`)
      fs.mkdirSync(dir)
      await copyDirFromAws(file, STATIC_FOLDER)
    }
  }
}

console.time('Start server...')

async function bootstrap() {
  const exists = await minioClient.bucketExists(s3Options.bucketName)
  if (!exists) {
    await minioClient.makeBucket(s3Options.bucketName, 'us-east-1')
  }

  fs.removeSync(STATIC_FOLDER)
  if (!fs.existsSync(STATIC_FOLDER)) {
    fs.mkdirSync(STATIC_FOLDER)
  }
  await copyDirFromAws('/', STATIC_FOLDER)
}

const app = express()
app.get('*', static({ root: STATIC_FOLDER }))

bootstrap()
  .then(filename => {
    app.get('*', static({
      root: STATIC_FOLDER, showDir: true, showDotfiles: false
    }))
    app.emit('ready')
    console.timeEnd('Start server...')
  })
  .catch(e => {
    console.error(e)
  })

module.exports = app