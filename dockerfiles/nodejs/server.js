process.env.NODE_ENV = 'production'

const app = require(process.env.APP || './awsStaticServer.js')
app.on('ready', () => {
  app.listen(process.env.PORT || 3001, () => {
    console.log('Example app listening on port 3001!')
  })
})