const express = require('express')
const static = require('serve-static')

const STATIC_FOLDER = `${__dirname}/.tmp/public`

const app = express()
app.get('*', static(STATIC_FOLDER))

process.nextTick(() => {
  app.emit('ready')
})

module.exports = app