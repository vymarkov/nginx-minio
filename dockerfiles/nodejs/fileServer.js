const {
  promisify
} = require('util')
const Minio = require('minio')

const fs = require('fs')

const express = require('express')
const S3FS = require('s3fs')
const uuid = require('uuid').v4

const s3Options = {
  endpoint: 'http://0.0.0.0:9001/foobar1',
  accessKeyId: 'admin',
  secretAccessKey: 'password',
  s3BucketEndpoint: true,
  signatureVersion: 'v4',
  bucketName: 'foobar1'
}
const s3fs = new S3FS(s3Options.bucketName, s3Options)
const minioClient = new Minio.Client({
  endPoint: '0.0.0.0',
  port: 9001,
  secure: false,
  accessKey: s3Options.accessKeyId,
  secretKey: s3Options.secretAccessKey
})
minioClient.makeBucket = promisify(minioClient.makeBucket)
minioClient._bucketExists = minioClient.bucketExists
minioClient.bucketExists = (bucketName) => {
  return new Promise((resolve, reject) => {
    return minioClient._bucketExists(bucketName, (err) => {
      if (err && err.code === 'NoSuchBucket') {
        return resolve(false)
      } else if (err) {
        return reject(err)
      }
      return resolve(true)
    })
  })
}

const app = express()

app.post('/file', (req, res) => {
  s3fs.writeFile(`${uuid()}.jpg`, fs.createReadStream('./fixtures/avatar.jpg'), (err) => {
    if (err) {
      return res.status(500).json({
        msg: 'Something went wrong!'
      })
    }
    res.status(204).json()
  })
})

function writeFile(filename = `${uuid()}.jpg`) {
  return new Promise((resolve, reject) => {
    s3fs.writeFile(filename, fs.createReadStream('./fixtures/avatar.jpg'), err => {
      if (err) {
        return reject(err)
      }
      resolve(filename)
    })
  })
}

function bootstrap() {
  return minioClient.bucketExists(s3Options.bucketName)
    .then(exits => {
      if (!exits) {
        return minioClient.makeBucket(s3Options.bucketName, 'us-east-1')
      }
    })
    .then(() => writeFile())
}

bootstrap()
  .then(filename => {
    app.get('/file', (req, res) => {
      s3fs.createReadStream(filename).pipe(res)
    })
    app.emit('ready')
  })
  .catch(e => {
    console.error(e)
  })

module.exports = app