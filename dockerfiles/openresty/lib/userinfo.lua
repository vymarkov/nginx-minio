local cjson = require("cjson")
local user = { id=1, username="mark", email="mark@gmail.com" }

ngx.header.content_type = "application/json"
ngx.say(cjson.encode(user))