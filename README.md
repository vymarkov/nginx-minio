# Nginx as reverse proxy for S3 (Minio)

## Motivation
This repo demonstrates how to use Nginx as reverse proxy together with Minio server as file storage for static unstructured content such html,css,javascript,images, etc files. Dockerfile for nginx-s3-proxy include nginx config with settings for ngx_aws_auth module, for proxy cache that help us to achive caching images,css,js,html so these files should be only downloaded once from Minio.

## Overview 
A small benchmark for use nginx as reverse proxy for Minio - a server with AWS S3 backward compatibility.
Main aim  

[![asciicast](https://asciinema.org/a/132393.png)](https://asciinema.org/a/132393)