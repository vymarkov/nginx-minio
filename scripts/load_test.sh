#!/bin/bash -ex

docker --version
docker-compose --version
docker-machine --version
docker info

docker-compose up -d minio nginx_proxy
docker-compose exec nginx_proxy sh -c "ulimit -n"
docker-compose exec minio sh -c "ulimit -n"
docker-compose run --rm console ./scripts/upload_fixtures.sh
docker-compose run --rm console siege -t 30s -i -b -c 100 http://cdn/avatar.jpg
docker-compose run --rm console ab -n 50000 -c 100 http://cdn/avatar.jpg
docker-compose down