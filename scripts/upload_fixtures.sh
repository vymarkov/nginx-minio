#!/bin/bash -e

s3="aws --endpoint-url http://minio:9000 s3"
$s3 rb s3://$AWS_S3_BUCKET --force && echo ok
$s3 mb s3://$AWS_S3_BUCKET
$s3 sync ./fixtures s3://$AWS_S3_BUCKET

$s3 rb s3://blog --force && echo ok
$s3 mb s3://blog
$s3 sync ./dockerfiles/openresty/myblog s3://$AWS_S3_BUCKET